import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Table from 'react-bootstrap/Table';
import { useState, useEffect } from 'react';
import axios from 'axios';

const baseUrl = 'https://api.nobelprize.org/2.1'

function App() {
  const [data, setData] = useState([]);
  // ------------------------------ ดึงข้อมูล --------------------------------
  useEffect(() => {
    axios.get(`${baseUrl}/nobelPrizes`).then((res) => {
      setData(res.data.nobelPrizes)
      console.log(data)
    })
  }, [])
  // ------------------------------ คัดกรองปีที่ได้รับรางวัล --------------------------------
  const awardYearFilter = [];
  for (let i = 0; i < data.length; i++) {
    if (awardYearFilter.indexOf(data[i].awardYear) < 0) {
      awardYearFilter.push(data[i].awardYear);
    }
  }
  // ------------------------------ ข้อมูลทั้งหมด -----------------------------------
  let [dataTable, setDataTable] = useState([]);
  
  // ------------------------------- ฟังกชันคำนวณผลรวม -----------------------
  let [prizeAmount, setPrizeAmount] = useState(0);

  const [startYear, setStartYear] = useState('');
  const [endYear, setEndYear] = useState('');
  function onStartYear(e) {
    setStartYear(e.target.value)
  }
  function onEndYear(e) {
    setEndYear(e.target.value)
  }
  function getDataTable() {
    const amount = data.reduce((acc, obj) => {
      return acc + obj.prizeAmount
    }, 0);
    setPrizeAmount(amount)
    setDataTable(data)
  }
  function getFilterYear() {
    console.log(startYear, 'ปีเริ่ม')
    console.log(endYear, 'ปีสิ้นสุด')
    const list = data.filter(x => startYear && endYear !== '' ? (x.awardYear) >= startYear && (x.awardYear) <= endYear : startYear !== '' ? (x.awardYear) >= startYear : endYear !== '' ? (x.awardYear) <= endYear : '')
    const amount = list.reduce((acc, obj) => {
      return acc + obj.prizeAmount
    }, 0);
    setPrizeAmount(amount)
    setDataTable(list)
  }
  const onFilter = (e) => {
    e.preventDefault();
    if (startYear !== '' || endYear !== '') {
      getFilterYear()
      console.log(dataTable)
    } else {
      getDataTable()
      console.log(dataTable)
    }
  }
  if (!data) return null
  // ------------------------------------- หน้าจอที่แสดง -------------------------------------------
  return (
    <>
      <Container>
        <Row className='my-5'>
          <Col className='text-center'>
          <h1>Nobel Prize</h1>
          </Col>
        </Row>
        <Row>
          <Col md={3}>
            <Form>
              <Form.Group>
                <Form.Label>ปีที่ได้รับรางวัล</Form.Label>
                <br></br>
                <Form.Label>- เริ่มต้นปี</Form.Label>
                <Form.Select value={startYear} onChange={onStartYear} className='mb-3' aria-label="Default select example">
                <option value=''>โปรดเลือกปี</option>
                {awardYearFilter.map((option) => (
                  // eslint-disable-next-line react/jsx-key
                  <option value={option}>{option}</option>
                ))}
                </Form.Select>
                <Form.Label>- สิ้นสุดปี</Form.Label>
                <Form.Select value={endYear} onChange={onEndYear} className='mb-3' aria-label="Default select example">
                <option value=''>โปรดเลือกปี</option>
                {awardYearFilter.map((option) => (
                  // eslint-disable-next-line react/jsx-key
                  <option value={option}>{option}</option>
                ))}
                </Form.Select>
              </Form.Group>
              <p>รวมเงินรางวัลทั้งหมด : {prizeAmount}</p>
            </Form>
            <Button className='w-100'variant="primary" onClick={onFilter}>Apply Filter</Button>
          </Col>
          <Col md={7} style={{ marginLeft: '7rem',}}>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>ชื่อรางวัล</th>
                <th>ปีที่ได้รับรางวัล</th>
                <th>ผู้ได้รับรางวัล</th>
                <th>แรงบันดาลใจของเขา</th>
                <th>เงินรางวัล</th>
              </tr>
            </thead>
            <tbody>
                {dataTable && dataTable.map((item, index) => (
                  <tr key={index}>
                  <td>{item.categoryFullName.en}</td>
                  <td>{item.awardYear}</td>
                  <td>{Object.keys(item.laureates).map((key) => {
                    if (item.laureates[key].fullName) {
                      return (
                        // eslint-disable-next-line react/jsx-key
                        <span>{item.laureates[key].fullName.en}</span>
                      )
                    } else {
                      return (
                        // eslint-disable-next-line react/jsx-key
                        <span>{item.laureates[key].orgName.en}</span>
                      )
                    }
                  })}</td>
                  <td>{item.category.en}</td>
                  <td>{item.prizeAmount}</td>
                </tr>
                ))}
            </tbody>
          </Table>
          </Col>
        </Row>
      </Container>
    </>
  )
}

export default App
