# imed-nobel-project

## Getting started

Install Node js (Recommended For Most Users)
https://nodejs.org/en

Install Visual Code + Extensions
https://code.visualstudio.com/

- ES7+ React/Redux/React-Native snippets
- ESLint
- Color Hightlight(optional)
- Code Snapshot(optional)
- Auto Rename Tag(optional)
- Prettier - Code Formatter

Open Command Prompt
```
git clone https://gitlab.com/surat4/imed-nobel-project.git
cd imed-nobel-project
npm install
npm run dev
```
## Code ประกอบไปด้วย
## ไฟล์ App.jsx
API NobelPrize
ประกอบไปด้วย get /nobelPrizes ดึงข้อมูลทั้งหมดของNobelPrize
```
*** const baseUrl = 'https://api.nobelprize.org/2.1'

useEffect(() => {
    axios.get(`${baseUrl}/nobelPrizes`).then((res) => {
      setData(res.data.nobelPrizes)
      console.log(data)
    })
  }, [])
```

ส่วนนี้ต้องการปีที่ได้รับรางวัล มาประกอบการเลือก Filter โดยการหาค่าซ้ำกันของข้อมูลปีทั้งหมด
```
const awardYearFilter = [];
  for (let i = 0; i < data.length; i++) {
    if (awardYearFilter.indexOf(data[i].awardYear) < 0) {
      awardYearFilter.push(data[i].awardYear);
    }
  }
```

ส่วนนี้แสดงข้อมูลทั้งหมด & แสดงข้อมูลผ่านการ filter ข้อมูลระหว่างเริ่มต้นปี-สิ้นสุด & prizeAmount รวมเงินรางวัลทั้งหมดตามข้อมูลที่แสดง
```
  let [dataTable, setDataTable] = useState([]);
    function getDataTable() {
        const amount = data.reduce((acc, obj) => {
        return acc + obj.prizeAmount
        }, 0);
        prizeAmount = amount
        setDataTable(data)
    }

  let prizeAmount = 0;

  const [startYear, setStartYear] = useState('');
  const [endYear, setEndYear] = useState('');
  function onStartYear(e) {
    setStartYear(e.target.value)
  }
  function onEndYear(e) {
    setEndYear(e.target.value)
  }
  function getFilterYear() {
    console.log(startYear, 'ปีเริ่ม')
    console.log(endYear, 'ปีสิ้นสุด')
    const list = data.filter(x => startYear && endYear !== '' ? (x.awardYear) >= startYear && (x.awardYear) <= endYear : startYear !== '' ? (x.awardYear) >= startYear : endYear !== '' ? (x.awardYear) <= endYear : '')
    const amount = list.reduce((acc, obj) => {
      return acc + obj.prizeAmount
    }, 0);
    prizeAmount = amount
    setDataTable(list)
  }
  const onFilter = (e) => {
    e.preventDefault();
    if (startYear !== '' || endYear !== '') {
      getFilterYear()
    } else {
      getDataTable()
    }
  }
```

เงื่อนไขข้อมูลไม่มีจะส่งค่าเป็น null
```
if (!data) return null
```

ส่วนนี้แสดงในหน้าจอ
มีการสร้าง Option โดยใช้การ map ค่าจากตัวแปร awardYearFilter ที่ทำการหาค่าซ้ำกันของข้อมูลปีทั้งหมด จะได้เป็น(1901/1902/1903/1904/1905)

มีการสร้าง Table โดยใช้การ map ค่าจากตัวแปร dataTable แสดงข้อมูลให้เห็นในตารางจะมีหัวข้อ (ชื่อรางวัล/ปีที่ได้รับรางวัล/ผู้ได้รับรางวัล/แรงบันดาลใจของเขา/เงินรางวัล)

```
return (
    <>
      <Container>
        <Row className='my-5'>
          <Col className='text-center'>
          <h1>Nobel Prize</h1>
          </Col>
        </Row>
        <Row>
          <Col md={3}>
            <Form>
              <Form.Group>
                <Form.Label>ปีที่ได้รับรางวัล</Form.Label>
                <br></br>
                <Form.Label>- เริ่มต้นปี</Form.Label>
                <Form.Select value={startYear} onChange={onStartYear} className='mb-3' aria-label="Default select example">
                <option value=''>โปรดเลือกปี</option>
                {awardYearFilter.map((option) => (
                  // eslint-disable-next-line react/jsx-key
                  <option value={option}>{option}</option>
                ))}
                </Form.Select>
                <Form.Label>- สิ้นสุดปี</Form.Label>
                <Form.Select value={endYear} onChange={onEndYear} className='mb-3' aria-label="Default select example">
                <option value=''>โปรดเลือกปี</option>
                {awardYearFilter.map((option) => (
                  // eslint-disable-next-line react/jsx-key
                  <option value={option}>{option}</option>
                ))}
                </Form.Select>
              </Form.Group>
              <p>รวมเงินรางวัลทั้งหมด : {prizeAmount}</p>
            </Form>
            <Button className='w-100'variant="primary" onClick={onFilter}>Apply Filter</Button>
          </Col>
          <Col md={7} style={{ marginLeft: '7rem',}}>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>ชื่อรางวัล</th>
                <th>ปีที่ได้รับรางวัล</th>
                <th>ผู้ได้รับรางวัล</th>
                <th>แรงบันดาลใจของเขา</th>
                <th>เงินรางวัล</th>
              </tr>
            </thead>
            <tbody>
                {dataTable && dataTable.map((item, index) => (
                  <tr key={index}>
                  <td>{item.categoryFullName.en}</td>
                  <td>{item.awardYear}</td>
                  <td>{Object.keys(item.laureates).map((key) => {
                    if (item.laureates[key].fullName) {
                      return (
                        // eslint-disable-next-line react/jsx-key
                        <span>{item.laureates[key].fullName.en}</span>
                      )
                    } else {
                      return (
                        // eslint-disable-next-line react/jsx-key
                        <span>{item.laureates[key].orgName.en}</span>
                      )
                    }
                  })}</td>
                  <td>{item.category.en}</td>
                  <td>{item.prizeAmount}</td>
                </tr>
                ))}
            </tbody>
          </Table>
          </Col>
        </Row>
      </Container>
    </>
  )
```